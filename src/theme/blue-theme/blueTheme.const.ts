export const blue = {

  '--primary' : '#8e24aa',
  '--primary-variant': '#ffffff',

  '--secondary': '#1565c0',
  '--secondary-variant': '#e8f5e9',

  '--background': '#f5f5f5',
  '--surface': '#ffffff',
  '--dialog': '#ffffff',
  '--alt-surface': '#eeeeee',
  '--alt-dialog': '#eeeeee',

  '--on-primary': '#ffffff',
  '--on-secondary': '#ffffff',
  '--on-background': '#000000',
  '--on-surface': '#000000',

  '--green': 'green',
  '--red': '#E60000',
  '--yellow': '#B8860B',
  '--blue': 'blue',
  '--purple': 'purple',
  '--light-green': 'lightgreen',
  '--grey': 'grey',
  '--grey-light': 'lightgrey',
  '--black': 'black',
  '--white': 'white',
  '--moderator': '#ffffff'

};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Blue',
      'de': 'Blau'
    },
    'description': {
      'en': 'Contrast compliant with WCAG 2.1 AA',
      'de': 'Helligkeitskontrast nach WCAG 2.1 AA'
    }
  },
  'isDark': false,
  'order': 0,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'secondary'

};












