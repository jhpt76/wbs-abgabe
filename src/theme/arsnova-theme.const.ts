import { dark, dark_meta } from './dark-theme/darkTheme.const';
import { arsnova, arsnova_meta } from './light-theme/light-theme';
import { purple, purple_meta } from './purple-theme/purpleTheme.const';
import { highcontrast, highcontrast_meta } from './high-contrast-theme/highContrastTheme.const';
import { blue, blue_meta } from './blue-theme/blueTheme.const';

export const themes = {
  arsnova: arsnova,
  dark: dark,
  projector: purple,
  highcontrast: highcontrast,
  blue: blue
};

export const themes_meta = {
  arsnova: arsnova_meta,
  dark: dark_meta,
  projector: purple_meta,
  highcontrast: highcontrast_meta,
  blue: blue_meta
};
